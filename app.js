window.onload = function() {
	var C = document.getElementById("C");
	var right = document.getElementById("right");
	var left = document.getElementById("left");
	var division = document.getElementById("division");
	var one = document.getElementById("one");
	var two = document.getElementById("two");
	var three = document.getElementById("three");
	var four = document.getElementById("four");
	var five = document.getElementById("five");
	var six = document.getElementById("six");
	var seven = document.getElementById("seven");
	var eight = document.getElementById("eight");
	var nine = document.getElementById("nine");
	var times = document.getElementById("times");
	var minus = document.getElementById("minus");
	var plus = document.getElementById("plus");
	var zero = document.getElementById("zero");
	var point = document.getElementById("point");
	var equal = document.getElementById("equal");
	
	C.onclick = function(){document.getElementById("screen").value = "0"};
	right.onclick = function(){
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = ")";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = ")";
		else
			document.getElementById("screen").value += ")";
	}
	left.onclick = function(){
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = "(";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = "(";
		else
			document.getElementById("screen").value += "(";
	}
	division.onclick = function() {
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = "/";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = "/";
		else
			document.getElementById("screen").value += "/";
	}
	one.onclick = function() {
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = "1";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = "1";
		else
			document.getElementById("screen").value += "1";
	}
	two.onclick = function() {
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = "2";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = "2";
		else
			document.getElementById("screen").value += "2";
	}
	three.onclick = function() {
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = "3";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = "3";
		else
			document.getElementById("screen").value += "3";
	}
	four.onclick = function() {
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = "4";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = "4";
		else
			document.getElementById("screen").value += "4";
	}
	five.onclick = function() {
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = "5";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = "5";
		else
			document.getElementById("screen").value += "5";
	}
	six.onclick = function() {
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = "6";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = "6";
		else
			document.getElementById("screen").value += "6";
	}
	seven.onclick = function() {
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = "7";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = "7";
		else
			document.getElementById("screen").value += "7";
	}
	eight.onclick = function() {
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = "8";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = "8";
		else
			document.getElementById("screen").value += "8";
	}
	nine.onclick = function() {
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = "9";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = "9";
		else
			document.getElementById("screen").value += "9";
	}
	times.onclick = function() {
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = "*";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = "*";
		else
			document.getElementById("screen").value += "*";
	}
	minus.onclick = function() {
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = "-";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = "-";
		else
			document.getElementById("screen").value += "-";
	}
	plus.onclick = function() {
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = "+";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = "+";
		else
			document.getElementById("screen").value += "+";
	}
	zero.onclick = function() {
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = "0";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = "0";
		else
			document.getElementById("screen").value += "0";
	}
	point.onclick = function() {
		if(document.getElementById("screen").value == "0")
			document.getElementById("screen").value = ".";
		else if(document.getElementById("screen").value == "error")
			document.getElementById("screen").value = ".";
		else
			document.getElementById("screen").value += ".";
	}
	equal.onclick = function() {
		try {
			eval(document.getElementById("screen").value); 
		} catch (e) {
			if (e instanceof SyntaxError) {
				document.getElementById("screen").value = "error";
			}
		}
		if(document.getElementById("screen").value != "error")
			document.getElementById("screen").value = eval(document.getElementById("screen").value);
	}
}